#!/usr/bin/env python
"""
A simple script to parse reads based on:
http://pathogenomics.bham.ac.uk/blog/2009/09/tips-for-de-novo-bacterial-genome-assembly/
    1) Trim reads for quality ---> not implemented, discard poor quality reads instead, see -q and -m
    2) Eliminate singletons ---> implemented, see -i and -d
    3) Throw out reads with N ---> implemented with modifications which include trimming first/last N, and trimming to remove 
        internal Ns
    4) Throw out reads with low average quality --> see #1
As well as a number of home-grown ideas:
    1) Hardclipping - allows user to indicate clippoint which will be applied to all reads
    2) Retain SE - keeps good single-end reads in order to improve coverage if needed
    3) Discard TruSeq reads - finds reads with at least 8 bases of TruSeq adaptors and discards them
        -this uses a fuzzy regular expression library, "tre" to allow one mismatch/indel in the match.
        -
    4) quality types - supports input of old and new Illumina quality types, see -t
    5) read-id reformatting - reformatts read IDs so that they are compatible with Newbler 2.6, but 
        retains information about 

NOTE: the "tre" package can be a bit tricky to install.
For fast, fuzzy regular expression support, install TRE: http://laurikari.net/tre/download/
    Note that you have to configure, make, make install, and then python setup.py install in order to get it to work properly.
    If installing locally use --prefix and modify LD_LIBRARY_PATH because Python needs to be able to find the libraries in order to
    install the module.


----------------------Copyright----------------------
Copyright (c) 2012 Sam S Hunter (shunter@gmail.com) under Artistic License 2.0
http://opensource.org/licenses/artistic-license-2.0.php
----------------------Copyright----------------------

"""

#import sys
from Bio import SeqIO
from collections import Counter
import time
import sys
import os
from optparse import OptionParser #http://docs.python.org/library/optparse.html

try:
    TruSeqSensitive = True
    import tre
except ImportError:
    TruSeqSensitive = False

global PE 

parser = OptionParser()
parser.add_option('-q',  '--qual', help = "minimum quality before a base is consider 'low-quality", 
    action="store", type = "int", dest = "MINQUAL", default = 20)
parser.add_option('-m', '--maxlowqual', help = "maximum number of low-quality bases before a read is discarded",
    action="store", type = "int", dest = "MAXLOWQUAL", default = 5)
parser.add_option('-d', '--maxduplicates', help = "maximum number of duplicate reads that will be written", 
    action="store", type = "int", dest = 'MAXDUPLICATES', default = 10)
parser.add_option('-i', '--minduplicates', help = "minimum number of duplicates before read will be written", 
    action="store", type = "int", dest = 'MINDUPLICATES', default = 1)
parser.add_option('-t', '--qualityType', help = "fastq-sanger (+33), fastq-solexa (+64), fastq-illumina (+64, old Illumina)", 
    action="store", type="str",dest="FASTQ", default="fastq-sanger")
parser.add_option('-c', '--clipends', dest = 'HARDCLIPS', default= 'F.0.0_R.0.0', help = "Format clips like F.2.10_R.3.15 to \
 clip the first 2 bases, and last 10 bases from the Forward read, and first 3 and last 15 from Reverse read. Use F.2.10 in SE mode.")

parser.usage = "\n\tSE Usage: %prog [options] input.fastq.gz out_directory basename"
parser.usage += "\n\tPE Usage: %prog [options] in_PE1.fastq.gz in_PE2.fastq.gz out_directory basename" 
parser.usage += "\n\tFor fast fuzzy pattern matching to remove reads with TruSeq adapters, ensure that the tre package is installed"
parser.usage += "\n\thttp://laurikari.net/tre/download/ "

parser.usage += """
\n
Interpreting the output:

13:26:42: Record: 100000, records per second=1855.68094214  <---- Time stamp and performance information
        Bases: 19029197, bases per second: 353121.157175  <---- Additional performance information
        lowqual:[45660, 66404]   <--- PE reads discarded for low quality, [PE1, PE2]
        hasN:[4, 130]   <--- PE reads discarded for containing Ns which could not be trimmed, [PE1, PE2]
        wrongName:0   <--- PE reads which had names that didn't match
        TruSeq adaptors: 7717   <--- PE reads were discarded because they contained TruSeq adapters
        PEs:26591   <--- PE reads which passed filter 
        SEs:[20413, 2237]   <--- SE reads which passed filter, [PE1, PE2]
        fail:[45664, 66534]  <--- Total number of reads with didn't pass filters [PE1, PE2]
        duplicates:[0, 0]   <--- Number of reads discarded for having more duplicates than allowed (-d parameter)
        firsts:[0, 0]  <--- Number of reads discarded for occuring only once (-i parameter). 
        Read1) leftNtrimmed: 0 rightNtrimmed:305  <--- Number of PE1 reads which had Ns trimmed from the left or right end.
        Read2) leftNtrimmed: 0 rightNtrimmed:0  <--- Number of PE2 reads which had Ns trimmed from the left or right end.
13:26:42: Timers:  <--- A set of timers which track the amount of time used in each processing step.
        N-trimming: 0.87s
        Qual-Trimming: 5.93s
        Duplicates: 2.48s
        TruSeq: 33.75s
        Writing: 0.90s
"""

(options,  args) = parser.parse_args() #uncomment this line for command line support

MINQUAL = options.MINQUAL
MAXLOWQUAL = options.MAXLOWQUAL
MAXDUPLICATES = options.MAXDUPLICATES
MINDUPLICATES = options.MINDUPLICATES
fastq = options.FASTQ

def process_reads(iter1, iter2, log_outf, outfPE, outfPE1, outfPE2, outfSE, basename):
    #parameters, modify for different behavior.
    #tracking variables:
    starttime = lasttime= time.time()
    readstats = {}
    readstats['lowqual'] = [0,0]
    readstats['hasN'] = [0,0]
    readstats['wrongName'] = 0
    readstats['written'] = [0,0]
    readstats['PEs'] = 0
    readstats['SEs'] = [0,0]
    readstats['fail'] = [0,0]
    readstats['duplicates'] = [0,0]
    readstats['first'] = [0,0]
    readstats['trimmedN'] = [0,0,0,0]  # Some reads have a single N on either end.. this should be easy to trim
    readstats['TruSeq'] = 0
    readstats['bases'] = 0
    timers = {}
    timers['N'] = 0.0
    timers['qual'] = 0.0
    timers['dups'] = 0.0
    timers['TruSeq'] = 0.0
    timers['write'] = 0.0
    readHash = Counter()
    record = 0
    #setup for TruSeq screening:
    patterns = ["AGATCGGAAGAGCACACGTCTG", "AGATCGGAAGAGCGTCGTGTA"]
    if TruSeqSensitive:
        txt = "Found 'tre' module, TruSeq screening running in sensitive mode."
        log(log_outf, txt)
        adaptor = "AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC"
        #Build a set of patterns using the first 22 characters of the TruSeq Adaptor 
        #Note that "$" anchors the pattern to the end of the sequence.
        #The idea is to find anything with 8 or more bases of the TruSeq Adaptor at the right end of the sequence,
        # hence the usage of multiple anchored REs.
        tmp = []
        for i in range(8,22):
            tmp.append(adaptor[0:i] + "$")
        tmp = '|'.join(tmp)
        tmp += "|GATCGGAAGAGCACACGTCT.*"
        tre_p = tre.compile(tmp, tre.EXTENDED)
        tre_fz = tre.Fuzzyness(delcost=1, inscost=1, subcost=1, maxerr=1)
    else:
        txt = "Could not find 'tre' module, TruSeq screening running in normal (exact pattern matching) mode."
        log(log_outf, txt)
    #Start processing reads.
    try:
        while 1:
            record += 1
            keep1 = True
            keep2 = True
            if not PE or iter2 is None:
                keep2 = False
            if record % 100000 == 0:
                txt = "Record: %s, records per second=%s" % (record, 100000/(time.time() - lasttime))
                txt += "\n\tBases: %s, bases per second: %s" % (readstats['bases'], readstats['bases'] / (time.time() - starttime))
                txt += "\n\tlowqual:%s\n\thasN:%s\n\twrongName:%s" % (readstats['lowqual'], readstats['hasN'], readstats['wrongName'])
                txt += "\n\tTruSeq adaptors: %s" % readstats['TruSeq']
                txt += "\n\tPEs:%s\n\tSEs:%s\n\tfail:%s" % (readstats['PEs'], readstats['SEs'], readstats['fail'])
                txt += "\n\tduplicates:%s\n\tfirsts:%s" % (readstats['duplicates'], readstats['first'])
                txt += "\n\tRead1) leftNtrimmed: %s rightNtrimmed:%s" % (readstats['trimmedN'][0], readstats['trimmedN'][1])
                txt += "\n\tRead2) leftNtrimmed: %s rightNtrimmed:%s" % (readstats['trimmedN'][2], readstats['trimmedN'][3])
                log(log_outf, txt)
                lasttime = time.time()
                txt = "Timers:\n\tN-trimming: %0.2fs\n\tQual-Trimming: %0.2fs\n\t" % (timers['N'], timers['qual'])
                txt += "Duplicates: %0.2fs\n\tTruSeq: %0.2fs\n\tWriting: %0.2fs" % (timers['dups'], timers['TruSeq'], timers['write'])
                log(log_outf, txt)
            read1 = iter1.next()
            if PE: read2 = iter2.next()
            #Check that reads have the same name:
            if PE and read1.name.split(" ")[0].split("#")[0] != read2.name.split(" ")[0].split("#")[0]:
                txt = "ERROR read names don't match, record %s, read IDs:%s, %s" % (record, read1.name, read2.name)
                log(log_outf, txt)
                readstats['wrongName'] += 1
                #continue
            #------------------------------------ HardClip reads ------------------------------------
            #Do any hard-trimming indicated:
            if HARDCLIPPING:
                read1 = read1[HARDCLIPS[0]:HARDCLIPS[1]]
                if PE: read2 = read2[HARDCLIPS[2]:HARDCLIPS[3]]
            #------------------------------------ Trim N's ------------------------------------
            #read1
            st = time.time()
            if keep1:
                firstn = read1.seq.find("N")
                if firstn != -1:
                    if firstn > 100:  # just trim off right end of the read if it is > 100bp and has 1 or more Ns 
                        read1 = read1[0:firstn]
                        readstats['trimmedN'][1] += 1
                    else:
                        if read1.seq.count("N") == 1:
                            if read1.seq[0] == "N":
                                read1 = read1[1:]
                                readstats['trimmedN'][0] += 1
                        else:
                            keep1 = False
                            readstats['hasN'][0] += 1 
                            readstats['fail'][0] += 1
            #read2
            if keep2:
                firstn = read2.seq.find("N")
                if firstn != -1:
                    if firstn > 100:  # just trim off right end of the read if it is > 100bp and has 1 or more Ns 
                        read2 = read2[0:firstn]
                        readstats['trimmedN'][1] += 1
                    else:
                        if read2.seq.count("N") == 1:
                            if read2.seq[0] == "N":
                                read2 = read2[1:]
                                readstats['trimmedN'][2] += 1
                        else:
                            keep2 = False
                            readstats['hasN'][1] += 1 
                            readstats['fail'][1] += 1
            timers['N'] += time.time() - st
            if keep1 == False and keep2 == False:
                continue  # if both fail N skip to the next pair of reads
            #------------------------------------ Low quality bases ------------------------------------
            #Check for low-quality bases:
            st = time.time()
            if keep1:
                q1 = sum(map(lambda x: x < MINQUAL, read1.letter_annotations['phred_quality']))
                if q1 > MAXLOWQUAL:
                    keep1 = False
                    readstats['lowqual'][0] += 1
                    readstats['fail'][0] += 1
            if keep2:
                q2 = sum(map(lambda x: x < MINQUAL, read2.letter_annotations['phred_quality'])) 
                if q2 > MAXLOWQUAL:
                    keep2 = False
                    readstats['lowqual'][1] += 1
                    readstats['fail'][1] += 1
            timers['qual'] += time.time() - st
            if keep1 == False and keep2 == False:
                continue
            #--------------------------------------Duplicate reads--------------------------------------------------
            #Track and filter on how many times a read occurs:
            st = time.time()
            if keep1:
                seq1 = str(read1.seq)
                seq1rc = str(read1.reverse_complement().seq)
                if seq1rc in readHash:
                    readHash[seq1rc] += 1
                    seq1c = readHash[seq1rc]
                else:
                    readHash[seq1] += 1
                    seq1c = readHash[seq1]
            if keep2:
                seq2 = str(read2.seq)
                seq2rc = str(read2.reverse_complement().seq)
                if seq2rc in readHash:
                    readHash[seq2rc] += 1
                    seq2c = readHash[seq2rc]
                else:
                    readHash[seq2] += 1
                    seq2c = readHash[seq2]
            if keep1 and seq1c < MINDUPLICATES:
                keep1 = False
                readstats['first'][0] += 1
            if keep1 and seq1c > MAXDUPLICATES:
                keep1 = False
                readstats['duplicates'][0] += 1

            if keep2 and seq2c < MINDUPLICATES:
                keep2 = False
                readstats['first'][1] += 1
            if keep2 and seq2c > MAXDUPLICATES:
                keep2 = False
                readstats['duplicates'][1] += 1

            timers['dups'] += time.time() - st
            if keep1 == False and keep2 == False:
                continue  # if both fail, skip to the next pair of reads

            #----------------------------------------TruSeq adapters-----------------------------------------------
            # Filter out reads which contain the TruSeq adapter: -----
            st = time.time()
            if keep1 or keep2:
                if read1.seq.find(patterns[0]) > 0 or PE and read2.seq.find(patterns[1]) > 0:
                    keep1 = False
                    keep2 = False
                    readstats['TruSeq'] += 1
                    #txt = "Found TruSeq adapters using find in read %s" % read1.id
                    #log(log_outf, txt)
                elif TruSeqSensitive:
                    if tre_p.search(read1.seq.tostring(), tre_fz) is not None:
                        keep1 = False
                        keep2 = False
                        readstats['TruSeq'] += 1
                        #txt = "Found TruSeq adapters using tre in read %s" % read1.id
                        #log(log_outf, txt)
            timers['TruSeq'] += time.time() - st
            if keep1 == False and keep2 == False:
                continue  # if both fail, skip to the next pair of reads

            #------------------------------------------------------------------------------------------------------------
            #Write out to interleaved with modified names if both pass, or as SE if either doesn't:
            #Note that in order for Newbler to accept these reads the names must be like: @ID:num1:num2:num3:num4#0/1
            st = time.time()
            if keep1 and keep2:
                read1.id = "%s_R%s:0:0:0:0#0/1 (%s)" % (basename,record, read1.id)
                read1.description = ""
                read2.id = "%s_R%s:0:0:0:0#0/2 (%s)" % (basename,record, read2.id)
                read2.description = ""
                #write to shuffled file:
                SeqIO.write([read1, read2], outfPE, "fastq" )
                #write to paired files:
                SeqIO.write(read1, outfPE1, "fastq" )
                SeqIO.write(read2, outfPE2, "fastq" )
                readstats['PEs'] += 1
                readstats['bases'] += len(read1) + len(read2)
                timers['TruSeq'] += time.time() - st
                continue
            if keep1:
                read1.id = "%s_R%s_read1 (%s)" % (basename,record, read1.id)
                read1.description = ""
                SeqIO.write(read1, outfSE, "fastq")
                readstats['SEs'][0] += 1
                readstats['bases'] += len(read1)
            if keep2:
                read2.id = "%s_R%s_read2 (%s)" % (basename,record, read2.id)
                read2.description = ""
                SeqIO.write(read2, outfSE, "fastq")
                readstats['SEs'][1] += 1
                readstats['bases'] += len(read2)
            timers['write'] += time.time() - st
    except StopIteration:
        pass
    finally:
        txt = "Processing finished:"
        txt += "\n\tRecord: %s, records per second: %s" % (record, record/(time.time() - starttime))
        txt += "\n\tBases: %s, bases per second: %s" % (readstats['bases'], readstats['bases'] / (time.time() - starttime))
        txt += "\n\tlowqual:%s\n\thasN:%s\n\twrongName:%s" % (readstats['lowqual'], readstats['hasN'], readstats['wrongName'])
        txt += "\n\tTruSeq adaptors: %s" % readstats['TruSeq']
        txt += "\n\tPEs:%s\n\tSEs:%s\n\tfail:%s" % (readstats['PEs'], readstats['SEs'], readstats['fail'])
        txt += "\n\tduplicates:%s\n\tfirsts:%s" % (readstats['duplicates'], readstats['first'])
        txt += "\n\tRead1) leftNtrimmed: %s rightNtrimmed:%s" % (readstats['trimmedN'][0], readstats['trimmedN'][1])
        txt += "\n\tRead2) leftNtrimmed: %s rightNtrimmed:%s" % (readstats['trimmedN'][2], readstats['trimmedN'][3])
        log(log_outf, txt)

        txt = "Timers:\n\tN-trimming: %0.2fs\n\tQual-Trimming: %0.2fs\n\t" % (timers['N'], timers['qual'])
        txt += "Duplicates: %0.2fs\n\tTruSeq: %0.2fs\n\tWriting: %0.2fs" % (timers['dups'], timers['TruSeq'], timers['write'])
        log(log_outf, txt)

def log(log_outf, txt):
    print("%s: %s" % (time.strftime("%H:%M:%S", time.localtime()),   txt))
    log_outf.write("%s \n\t %s\n" % (time.strftime("%H:%M:%S", time.localtime()),  txt))
    log_outf.flush()


##### Start main part of program:
lasttime = starttime = time.time()
#print(args)

if len(args) < 3:
    print "Error, not enough parameters. Exiting."
    parser.print_help()
    sys.exit()

infile1 = os.path.realpath(args[0])

if len(args) == 3:
    print "Single input file detected, operating in SE mode."
    PE = False
    outdir = os.path.realpath(args[1])
    basename = args[2]

if len(args) == 4:
    print "Two input files detected, operating in PE mode."
    infile2 = os.path.realpath(args[1])
    outdir = os.path.realpath(args[2])
    basename = args[3]
    PE =True

if len(args) > 4:
    print "Error, too many parameters. Exiting."
    parser.print_help()
    sys.exit()

#Check inputs:
if not os.path.isfile(infile1):
    print("Can't find input file 1: %s" % infile1)
    sys.exit()
if PE and not os.path.isfile(infile2):
    print("Can't find input file 2: %s" % infile2)
    sys.exit()

#Check output directory:
if not os.path.exists(outdir):
    print("ERROR: The out-directory %s doesn't exist." % outdir)
    sys.exit()

log_outf = open(os.path.join(outdir, "_".join([basename, "read_QC.log"])), 'w')

#Parse HARDCLIPS:
HARDCLIPPING = False
if PE:
    HARDCLIPS = [0,0,0,0]
    if options.HARDCLIPS != 'F.0.0_R.0.0':
        h = options.HARDCLIPS.split("_")
        HARDCLIPPING = True
        HARDCLIPS[0] = int(h[0].split(".")[1])
        HARDCLIPS[1] = -int(h[0].split(".")[2])
        HARDCLIPS[2] = int(h[1].split(".")[1])
        HARDCLIPS[3] = -int(h[1].split(".")[2])
        if(HARDCLIPS[0] == 0): HARDCLIPS[0] = None 
        if(HARDCLIPS[1] == 0): HARDCLIPS[1] = None
        if(HARDCLIPS[2] == 0): HARDCLIPS[2] = None
        if(HARDCLIPS[3] == 0): HARDCLIPS[3] = None
        txt = "HARDCLIPS found, using: Forward: L %s, R %s;  Reverse: L %s, R %s" % (HARDCLIPS[0], HARDCLIPS[1], HARDCLIPS[2], HARDCLIPS[3])
        log(log_outf, txt)
if not PE:
    HARDCLIPS = [0,0]
    if options.HARDCLIPS != 'F.0.0_R.0.0':
        if len(options.HARDCLIPS.split("_")) > 1:
            print "In SE mode, please specify hardclips with F.X.Y format."
            sys.exit()
        HARDCLIPPING = True
        HARDCLIPS[0] = int(options.HARDCLIPS.split(".")[1])
        HARDCLIPS[1] = -int(options.HARDCLIPS.split(".")[2])
        if(HARDCLIPS[0] == 0): HARDCLIPS[0] = None 
        if(HARDCLIPS[1] == 0): HARDCLIPS[1] = None
        txt = "HARDCLIPS found, using: L %s, R %s" % (HARDCLIPS[0], HARDCLIPS[1])
        log(log_outf, txt)

txt = "Starting computation."
log(log_outf, txt)

txt = "Writing logs to: %s" % os.path.join(outdir, "_".join([basename, "read_QC.log"]))
log(log_outf, txt)

txt = "Using parameters: \n\tMINQUAL=%s\n\tMAXLOWQUAL=%s\n\tMAXDUPLICATES=%s\n\tMINDUPLICATES=%s" % (MINQUAL,MAXLOWQUAL,MAXDUPLICATES,MINDUPLICATES)
log(log_outf, txt)

if PE:
    outfPE = open(os.path.join(outdir, "_".join([basename, "PE_shuffled.fastq"])), 'w')
    outfPE1 = open(os.path.join(outdir, "_".join([basename, "PE1.fastq"])), 'w')
    outfPE2 = open(os.path.join(outdir, "_".join([basename, "PE2.fastq"])), 'w')
    outfSE = open(os.path.join(outdir, "_".join([basename, "SE.fastq"])), 'w')
if not PE:
    outfPE = None
    outfPE1 = None
    outfPE2 = None
    outfSE = open(os.path.join(outdir, "_".join([basename, "SE.fastq"])), 'w')

#Open inputs:
if not PE:
    iter2 = None
if infile1.split(".")[-1] == "gz":
    log(log_outf, "Input appears to be gzipped.")
    import gzip
    iter1 = SeqIO.parse(gzip.open(infile1, 'rb'), fastq)
    if PE: iter2 = SeqIO.parse(gzip.open(infile2, 'rb'), fastq)
elif infile1.split(".")[-1] == "bz2":
    log(log_outf, "Input appears to be bzip2.")
    log(log_outf, "WARNING, bzip2 often fails to read properly, use gzip if errors are reported.")
    import bz2
    iter1 = SeqIO.parse(bz2.BZ2File(infile2, 'r'), fastq)
    if PE: iter2 = SeqIO.parse(bz2.BZ2File(infile2, 'r'), fastq)
elif infile1.split(".")[-1] == "fastq":
    log(log_outf, "Input appears to be uncompressed fastq.")
    iter1 = SeqIO.parse(open(infile1, 'r'), fastq)
    if PE: iter2 = SeqIO.parse(open(infile2, 'r'), fastq)
else:
    log(log_outf, "File extension not recognized, assuming uncompressed fastq")
    iter1 = SeqIO.parse(open(infile1, 'r'), fastq)
    if PE: iter2 = SeqIO.parse(open(infile2, 'r'), fastq)


process_reads(iter1, iter2, log_outf, outfPE, outfPE1, outfPE2, outfSE, basename)

log_outf.close()
outfSE.close()
if PE:
    outfPE.close()
    outfPE1.close()
    outfPE2.close()
