#!/usr/bin/env python

"""
Demultiplex reads when they come with a "barcode" read and haven't been split previously.
"""

import sys
from Bio import SeqIO
import os


#------------------- functions ------------------------------
def strdist(s1, s2):
    if len(s1) == len(s2) and len(s1) > 0:
        return sum(map(lambda x: x[0] != x[1], zip(s1,s2) )) # python is bad-ass
    else:
        print "ERROR lengths of barcodes and index read do not match!"
        print "Target", s1
        print "Index read:", s2
        sys.exit()



#-----------------------------------------------------------

## Testing stuff:
# sys.argv.append("Target_file_Oregon_Tank_Sep_2012.txt")
# sys.argv.append("lane8_NoIndex_L008_R1.fastq.gz")
# sys.argv.append("lane8_NoIndex_L008_R3.fastq.gz")
# sys.argv.append("lane8_NoIndex_L008_R2.fastq.gz")



#----------- introductory stuff to check input-----------------
if len(sys.argv) not in [4,5]:
    print "Usage:"
    print "\tFor PE: Illumina_demultiplexer.py targets.tsv read1.fastq<.gz> read2.fastq<.gz> barcode.fastq<.gz>"
    print "\tFor SE: Illumina_demultiplexer.py targets.tsv read1.fastq<.gz> barcode.fastq<.gz>"
    print "\t Where: "
    print "\t\tfastq files can be gzipped or not but must end in .gz if they are gzipped."
    print "\t\ttargets.tsv must be TAB seperated, and have column names (sample_id<tab>barcode)"
    sys.exit()

#--------Get file names--------------
targetsf = sys.argv[1]
PE1f = sys.argv[2]

if len(sys.argv) == 5:
    PE2f = sys.argv[3]
    barcodef = sys.argv[4]
    PE=True 
else:
    barcodef = sys.argv[3]
    PE=False

#--------- Check that all inputs exist, report error and exit if not -----------
if not os.path.isfile(targetsf):
    print targetsf, " file cannot be found, please check input parameters and try again."
    sys.exit()


if not os.path.isfile(PE1f):
    print PE1f, " file cannot be found, please check input parameters and try again."
    sys.exit()

if not os.path.isfile(barcodef):
    print barcodef, " file cannot be found, please check input parameters and try again."
    sys.exit()

if PE and not os.path.isfile(PE2f):
    print PE2f, " file cannot be found, please check input parameters and try again."
    sys.exit()

#-----------Create file handles for Fastq/gz files -------------------
if PE1f.split(".")[-1] == "gz":
    print "Input appears to be gzipped."
    import gzip
    iter1 = SeqIO.parse(gzip.open(PE1f, 'rb'), "fastq")
    if PE: iter2 = SeqIO.parse(gzip.open(PE2f, 'rb'), "fastq")
    iteridx = SeqIO.parse(gzip.open(barcodef, 'rb'), "fastq")
else:
    print "Files appear to be uncompressed fastq."
    iter1 = SeqIO.parse(open(PE1f, 'r'), "fastq")
    if PE: iter2 = SeqIO.parse(open(PE2f, 'r'), "fastq")
    iteridx = SeqIO.parse(open(barcodef, 'r'), "fastq")

#---------Open and parse the targets file -------------
targets = {}  # barcode is key, sample_id is value
inf = open(targetsf, 'r')
l = inf.next()
l = l.strip().split('\t')
if l[0] != 'sample_id' or l[1] != 'barcode':
    print "Targets file is not properly formatted, first line must be: sample_id<tab>barcode<additional field optional>"
    sys.exit()

for l in inf:
    l = l.strip().split("\t")
    targets[l[1]] = l[0]

# ------- Open output files -----------
outfiles = {}

for k in targets:
    outfiles[k] = [gzip.open("%s_%s_PE1.fastq.gz" % (targets[k], k), 'wb'), gzip.open("%s_%s_PE2.fastq.gz" % (targets[k], k), 'wb')]


# ------- make some counters for reporting ------------
counters = {}
for k in targets:
    counters[k] = [0,0]  # first value is perfect matches, second is 1bp mismatch

counters['undetermined'] = 0

reads = 0
# Now iterate through both input files, split and write out
try:
    while 1:
        read1 = iter1.next()
        if PE: read2 = iter2.next()
        idx = iteridx.next()
        reads += 1

        k = None

        if idx.seq.tostring() in outfiles:
            k = idx.seq.tostring()
            counters[k][0] += 1   
        else:  # handle 1 base mismatches gracefully
            for key in outfiles:
                if strdist(key, idx.seq.tostring()) <= 1:
                    k = key
                    counters[k][1] += 1
        if k is not None:  
            SeqIO.write(read1, outfiles[k][0], "fastq" )                          
            SeqIO.write(read2, outfiles[k][1], "fastq" )                          
        else:
            counters['undetermined'] += 1

        if reads % 10000 == 0:
            print"-------------------"
            print "%s reads processed." % reads
            for k in targets:
                print "%s_%s: perfect match:%s, mismatch:%s" % (targets[k], k, counters[k][0], counters[k][1])
            print "undetermind: %s" % counters['undetermined']
            print"-------------------"            

except StopIteration:
    pass
finally:
    print "Finished processing "
    print"-------------------"
    print "%s reads processed."
    for k in targets:
        print "%s_%s: perfect match:%s, mismatch:%s" % (targets[k], k, counters[k][0], counters[k][1])
    print "undetermind: %s" % counters['undetermined']
    print"-------------------"            
